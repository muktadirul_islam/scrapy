# -*- coding: utf-8 -*-
import scrapy


class BasicSpider(scrapy.Spider):
    name = 'basic'
    allowed_domains = ['http://quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/random']

    def parse(self, response):
        quotes = response.xpath('/html/body/div/div[2]/div[1]/div/span[1]/text()').extract()
        writer = response.xpath('/html/body/div/div[2]/div[1]/div/span[2]/small/text()').extract()
        data = {'quotes':quotes,'name':writer}
        yield data
